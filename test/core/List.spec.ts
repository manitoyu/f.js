import { expect } from 'chai'
import List from '../../src/core/List'

describe('List', () => {
	const list = List(1, 2, 3)

	it('toArray', () => {
		expect(list.toArray()).to.deep.equal([1, 2, 3])
	})
})