import HKT from '../core/HKT'
import Monoid from './Monoid'
import { endoMonoid, dual } from '../instances/Monoids'
import { List } from '../core/List'

abstract class Foldable<F> {
	foldMap<A, B>(as: HKT<F, A>): (f: (a: A) => B) => (mb: Monoid<B>) => B {
		return f => mb => this.foldRight<A, B>(as)(mb.zero)((a, b) => mb.op(f(a), b))
	}

	foldLeft<A, B>(as: HKT<F, A>): (z: B) => (f: (b: B, a: A) => B) => B {
		return z => f => this.foldMap<A, (b: B) => B>(as)((a: A) => (b: B) => f(b, a))(dual(endoMonoid as Monoid<(b: B) => B>))(z)
	}

	foldRight<A, B>(as: HKT<F, A>): (z: B) => (f: (a: A, b: B) => B) => B {
		return z => f => this.foldMap<A, (b: B) => B>(as)((a: A) => (b: B) => f(a, b))(endoMonoid as Monoid<(b: B) => B>)(z)
	}

	concatenate<A>(as: HKT<F, A>): (m: Monoid<A>) => A {
		return m => this.foldLeft<A, A>(as)(m.zero)(m.op)
	}
}

export default Foldable