export default interface HKT<F, A> {
	__hkt: F
	__hkta: A
}

export interface HKT2<F, B, A> extends HKT<F, A> {
	__hktb: B
} 
