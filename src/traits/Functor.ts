import HKT from './HKT'

abstract class Functor<F> {
	abstract map<A, B>(fa: HKT<F, A>): (f: (a: A) => B) => HKT<F, B>
}

export default Functor