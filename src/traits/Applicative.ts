import Functor from './Functor'
import HKT from './HKT'

abstract class Applicative<F> extends Functor<F> {
	abstract unit<A>(a: () => A): HKT<F, A>

	abstract map2<A, B, C>(fb: HKT<F, A>): (fb: HKT<F, B>) => (f: (a: A, b: B) => C) => HKT<F, C>
	
	map<A, B>(fa: HKT<F, A>): (f: (a: A) => B) => HKT<F, B> {
		return f => this.map2<A, null, B>(fa)(this.unit(() => null))((a, _) => f(a))
	}
}

export default Applicative