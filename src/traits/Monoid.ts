interface Monoid<A> {
	zero: A
	op(a1: A, a2: A): A
}

export default Monoid