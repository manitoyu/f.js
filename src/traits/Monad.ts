import Applicative from './Applicative'
import HKT from './HKT'

abstract class Monad<F> extends Applicative<F> {
	abstract unit<A>(a: () => A): HKT<F, A>

	abstract flatMap<A, B>(ma: HKT<F, A>): (f: (a: A) => HKT<F, B>) => HKT<F, B>

	map<A, B>(ma: HKT<F, A>): (f: (a: A) => B) => HKT<F, B> {
		return f => this.flatMap<A, B>(ma)(a => this.unit(() => f(a)))
	}

	map2<A, B, C>(ma: HKT<F, A>): (mb: HKT<F, B>) => (f: (a: A, b: B) => C) => HKT<F, C> {
		return mb => (f: (a: A, b: B) => C) => this.flatMap<A, C>(ma)(a => this.map<B, C>(mb)(b => f(a, b)))
	}
}

export default Monad
