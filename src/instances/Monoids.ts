import Monoid from '../traits/Monoid'

export const intAddition = new class implements Monoid<number> {
	zero = 0
	op(a1: number, a2: number) {
		return a1 + a2
	}
}

export const intMultiplication = new class implements Monoid<number> {
	zero = 1
	op(a1: number, a2: number) {
		return a1 * a2
	}
}

export const booleanOr = new class implements Monoid<boolean> {
	zero = false
	op(a1: boolean, a2: boolean) {
		return a1 || a2
	}
}

export const booleanAnd = new class implements Monoid<boolean> {
	zero = true
	op(a1: boolean, a2: boolean) {
		return a1 && a2
	}
}

export const endoMonoid = new class<A> implements Monoid<(a: A) => A> {
	zero = (a: A) => a
	op(a1: (a: A) => A, a2: (a: A) => A) {
		return (a: A) => a1(a2(a))
	}
}

export function dual<A>(m: Monoid<A>) {
	return new class implements Monoid<A> {
		zero = m.zero
		op(a1: A, a2: A) {
			return m.op(a2, a1)
		}
	}
}
