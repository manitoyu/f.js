// import { Some, None } from './core/Option'
// import { ops } from './core/Operators'
import { List, Nil, listFoldable, listMonoid } from './core/List'
import State from './core/State'
// import { Left, Right } from './core/Either'
// import { Simple, int } from './core/RNG'

import { int, Simple } from './core/RNG'

const result = List.of(1, 2, 3)
	.flatMap(x => List.of(4, 5, 6)
		.flatMap(y => List.of(7, 8, 9)
			.flatMap(z => {
				return List.of(x, y, z)
			})))

console.log(result)


const v = int().flatMap(x => 
	int().flatMap(y =>
		int().map(z => [x, y, z])
	))
	.run(new Simple(new Date().getTime()))

console.log(v)