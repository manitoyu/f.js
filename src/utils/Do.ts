function Do(f: () => IterableIterator<any>) {
	const gen = f()

	const go = (v: any) => {
		let result = gen.next(v)
		console.log(result)
		if (!result.done) {
			return result.value.flatMap((v: any) => go(v))
		} else {
			return result.value
		}
	}

	return go(undefined)
}

export default Do