export default function assert(expr: boolean, msg: string) {
  if (!expr) throw new Error(msg)
}