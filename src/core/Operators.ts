import HKT from './HKT'

export interface IFunctor<F, A> extends HKT<F, A> {
	map<B>(f: (a: A) => B): HKT<F, B>
	map2<B, C>(fb: HKT<F, B>): (f: (a: A, b: B) => C) => HKT<F, C>
}

export class Operators {
	map<F, A, B>(f: (a: A) => B): (fa: IFunctor<F, A>) => IFunctor<F, B> {
		return fa => fa.map(f) as IFunctor<F, B>
	}

	map2<F, A, B, C>(f: (a: A, b: B) => C) {
		return (fa: IFunctor<F, A>) => (fb: IFunctor<F, B>) => {
			return fa.map2(fb)(f)
		}
	}
}

export const ops = new Operators()
