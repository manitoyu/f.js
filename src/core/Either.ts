import Monad from './Monad'

type F = 'Either'

declare module './Either' {
	interface Either<E, A> {
		map<B>(f: (a: A) => B): Either<E, B>
	}
}

abstract class Either<E, A> extends Monad<F, A> {
}

class Left<E> extends Either<E, null> {
	constructor(public get: E) {
		super()
	}

	unit<A>(a: () => A): Either<E, null> {
		return new Left(a())
	}

	flatMap<B>(f: (a: any) => Either<F, B>): Either<E, null> {
		return new Left(this.get)
	}
}

class Right<A> extends Either<null, A> {
	constructor(public get: A) {
		super()
	}

	unit<A>(a: () => A): Either<null, A> {
		return new Right(a())
	}

	flatMap<B>(f: (a: A) => Either<F, B>): Either<null, B> {
		return f(this.get)
	}
}

function applyLeft(e: any) {
	return new Left(e)
}

function applyRight(a: any) {
	return new Right(a)
}

export {
	applyLeft as Left,
	applyRight as Right
}