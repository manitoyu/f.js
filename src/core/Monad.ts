import Applicative from './Applicative'
import HKT from './HKT'

abstract class Monad<F, A> extends Applicative<F, A> {
	abstract unit<A>(a: () => A): HKT<F, A>

	abstract flatMap<B>(f: (a: A) => HKT<F, B>): HKT<F, B>

	map<B>(f: (a: A) => B): HKT<F, B> {
		return this.flatMap(a => this.unit(() => f(a)))
	}

	map2<B, C>(mb: HKT<F, B>): (f: (a: A, b: B) => C) => HKT<F, C> {
		return (f: (a: A, b: B) => C) => this.flatMap(a => (mb as Monad<F, B>).map(b => f(a, b)))
	}
}

export default Monad
