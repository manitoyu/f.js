import HKT from './HKT'

abstract class Functor<F, A> implements HKT<F, A> {
	__hkt: F
	__hkta: A

	abstract map<B>(f: (a: A) => B): HKT<F, B>
}

export default Functor