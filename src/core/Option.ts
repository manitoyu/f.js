import Monad from './Monad'
import HKT from './HKT'

type F = 'Option'

abstract class Option<A> extends Monad<F, A> {
	
}

class Some<A> extends Option<A> {
	constructor(public value: A) {
		super()
	}

	unit<A>(a: () => A): Option<A> {
		return new Some(a())
	}
	
	flatMap<B>(f: (a: A) => Option<B>): Option<B> {
		return f(this.value)
	}
}

class None<A> extends Option<A> {
	unit<A>(a: () => A): Option<A> {
		return new None()
	}
	
	flatMap<B>(f: (a: A) => Option<B>): Option<B> {
		return new None()
	}
}

function applySome<A>(value: A) {
	return new Some<A>(value)
}

function applyNone() {
	return new None<any>()
}

export {
	applySome as Some,
	applyNone as None
}