import State from './State'

interface RNG {
	nextInt(): [number, RNG]
}

type Rand<A> = State<RNG, A>

class Simple implements RNG {
	constructor(public seed: number) {
	}

	nextInt(): [number, RNG] {
		const newSeed = (this.seed * 0x5DEECE66D + 0xB) & 0xFFFFFFFFFFFF
		const nextRNG = new Simple(newSeed)
		const n = parseInt(String(newSeed >>> 16))
		return [n, nextRNG]
	}
}

function int(): Rand<number> {
	return new State(s => s.nextInt())
}

export {
	RNG,
	Rand,
	Simple,
	int
}