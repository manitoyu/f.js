import Monad from './Monad'
import HKT from './HKT'

type F = 'Cons'

declare module './Operators' {
	interface Operators {
		map<F, A, B>(f: (a: A) => B): (fa: HKT<F, A>) => Cons<B>
	}
}

class Cons<A> extends Monad<F, A> {
	private h: () => A
	private t: () => Cons<A>

	constructor(h: () => A, t: () => Cons<A>) {
		super()
		this.h = h
		this.t = t
	}

	unit<A>(a: () => A): HKT<F, A> {
		return cons(a, () => null)
	}

	flatMap<B>(f: (a: A) => Cons<B>): Cons<B> {
		return this.foldRight(null)((h, t) => {
			return cons(() => f(h).h(), t)
		})
	}

	toArray() {
		const self = this
		function go(l: Cons<A>, acc: Array<A>): Array<A> {
			if (l == null) return acc
			return go(l.t(), acc.concat(l.h()))
		}
		return go(this, [])
	}

	foldRight<B>(z: B) {
		return (f: (a: A, b: () => B) => B): B => {
			return f(this.h(), () => {
				const t = this.t()
				return t == null ? null : t.foldRight(z)(f)
			})
		}
	}

	// map<B>(f: (a: A) => B): Cons<B> {
	// 	// return this.foldRight(null)((h, t) => cons(() => f(h), t))
	// 	return super.map<B>(this)(f)
	// }

	map<B>(f: (a: A) => B): Cons<B> {
		return super.map<B>(f) as Cons<B>
	}

	unshift(a: A): Cons<A> {
		return cons(() => a, () => this)
	}
}

function cons<A>(hd: () => A, tl: () => Cons<A>) {
	return new Cons(() => hd(), () => tl())
}

function apply<A>(...as: A[]): Cons<A> {
	if (as.length == 0) return null
	return cons(() => as[0], () => apply(...as.slice(1)))
}


export {
	apply as Stream
}