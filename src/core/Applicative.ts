import Functor from './Functor'
import HKT from './HKT'

abstract class Applicative<F, A> extends Functor<F, A> {
	abstract unit<A>(a: () => A): HKT<F, A>

	abstract map2<B, C>(fb: HKT<F, B>): (f: (a: A, b: B) => C) => HKT<F, C>
	
	map<B>(f: (a: A) => B): HKT<F, B> {
		return this.map2<null, B>(this.unit(() => null))((a, _) => f(a))
	}
}

export default Applicative