import { HKT2 } from '../traits/HKT'
import Monad from '../traits/Monad'

type F = 'State'

class State<S, A> {
	__hkt: F
	__hkta: A

	constructor(public run: (s: S) => [A, S]) {
	}

	static of<S, A>(run: (s: S) => [A, S]) {
		return new State<S, A>(run)
	}

	unit(a: () => A): State<S, A> {
		return stateMonad.unit(a) as State<S, A>
	}

	map<B>(f: (a: A) => B): State<S, B> {
		return stateMonad.map(this)(f) as State<S, B>
	}

	flatMap<B>(f: (a: A) => State<S, B>): State<S, B> {
		return stateMonad.flatMap(this)(f) as State<S, B>
	}

	map2<B, C>(mb: State<S, B>): (f: (a: A, b: B) => C) => State<S, C> {
		return f => stateMonad.map2(this)(mb)(f) as State<S, C>
	}
}

const stateMonad = new class<S, _> extends Monad<F>{
	unit<A>(a: () => A): State<S, A> {
		return new State(s => [a(), s])
	}

	flatMap<A, B>(ma: State<S, A>): (f: (a: A) => State<S, B>) => State<S, B> {
		return f => new State(s => {
			const [a, s1] = ma.run(s)
			return f(a).run(s1)
		})
	}
}

export default State

export {
	stateMonad
}