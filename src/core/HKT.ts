export default interface HKT<F, A> {
	__hkt: F
	__hkta: A
}
