import HKT from '../traits/HKT'
import Monoid from '../traits/Monoid'
import Monad from '../traits/Monad'
import Foldable from '../traits/Foldable'

type F = 'List'

class List<A> {
	h: A
	t: List<A>

	__hkt: F
	__hkta: A

	static of<A>(...xs: A[]): List<A> {
		return apply(...xs)
	}

	unit<A>(a: () => A): List<A> {
		return listMonad.unit(a)
	}

	map<B>(f: (a: A) => B): List<B> {
		return listMonad.map(this)(f) as List<B>
	}
	
	flatMap<B>(f: (a: A) => List<B>): List<B> {
		return listMonad.flatMap(this)(f) as List<B>
	}

	foldLeft<B>(z: B): (f: (b: B, a: A) => B) => B {
		return f => listFoldable.foldLeft(this)(z)(f) as B
	}

	foldRight<B>(z: B): (f: (a: A, b: B) => B) => B {
		return f => listFoldable.foldRight(this)(z)(f) as B
	}

	concatenate(l: List<List<A>>): List<A> {
		return listFoldable.concatenate(l)(listMonoid as Monoid<List<A>>)
	}

	append(l: List<A>): List<A> {
		return listFoldable.concatenate(List.of(this, l))(listMonoid as Monoid<List<A>>)
	}
}

const Nil = new class Nil extends List<any> {
}

class Cons<A> extends List<A> {
	constructor(h: A, t: Cons<A>) {
		super()

		this.h = h
		this.t = t
	}
}

function apply<A>(...xs: A[]): List<A> {
	return xs.length == 0
		? Nil
		: new Cons(xs[0], apply(...xs.slice(1)) as Cons<A>)
}

export const listMonoid = new class<A> implements Monoid<List<A>> {
	zero = Nil
	op(a1: List<A>, a2: List<A>) {
		return listFoldable.foldRight<A, List<A>>(a1)(a2)((a, b) => new Cons(a, b))
	}
}

export const listMonad = new class extends Monad<F> {
	unit<A>(a: () => A): List<A> {
		return List.of(a())
	}
	
	flatMap<A, B>(ma: List<A>): (f: (a: A) => List<B>) => List<B> {
		return f => listFoldable.foldRight<A, List<B>>(ma)((listMonoid as Monoid<List<B>>).zero)((a, b) => (listMonoid as Monoid<List<B>>).op(f(a), b))
	}
}

export const listFoldable = new class extends Foldable<F> {
	foldRight<A, B>(as: List<A>): (z: B) => (f: (a: A, b: B) => B) => B {
		return z => f => {
			function go(l: List<A>, acc: Array<A>): Array<A> {
				if (l == Nil) return []
				if (l.t != Nil) {
					return go(l.t, [l.h, ...acc])
				}
				return [l.h, ...acc]
			}

			return go(as, []).reduce((b, a) => f(a, b), z)
		}
	}
}

export {
	List,
	Nil,
	F,
	Cons
}
