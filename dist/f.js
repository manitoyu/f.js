(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Applicative_1 = __webpack_require__(3);
var Monad = (function (_super) {
    __extends(Monad, _super);
    function Monad() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Monad.prototype.map = function (f) {
        var _this = this;
        return this.flatMap(function (a) { return _this.unit(function () { return f(a); }); });
    };
    Monad.prototype.map2 = function (mb) {
        var _this = this;
        return function (f) { return _this.flatMap(function (a) { return mb.map(function (b) { return f(a, b); }); }); };
    };
    return Monad;
}(Applicative_1.default));
exports.default = Monad;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(2));
__export(__webpack_require__(5));
__export(__webpack_require__(6));


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Monad_1 = __webpack_require__(0);
var Option = (function (_super) {
    __extends(Option, _super);
    function Option() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Option.prototype.map = function (f) {
        return _super.prototype.map.call(this, f);
    };
    Option.prototype.map2 = function (fb) {
        var _this = this;
        return function (f) {
            return _super.prototype.map2.call(_this, fb)(f);
        };
    };
    return Option;
}(Monad_1.default));
var Some = (function (_super) {
    __extends(Some, _super);
    function Some(value) {
        var _this = _super.call(this) || this;
        _this.value = value;
        return _this;
    }
    Some.prototype.unit = function (a) {
        return new Some(a());
    };
    Some.prototype.flatMap = function (f) {
        return f(this.value);
    };
    return Some;
}(Option));
var None = (function (_super) {
    __extends(None, _super);
    function None() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    None.prototype.unit = function (a) {
        return new None();
    };
    None.prototype.flatMap = function (f) {
        return new None();
    };
    return None;
}(Option));
function applySome(value) {
    return new Some(value);
}
exports.Some = applySome;
function applyNone() {
    return new None();
}
exports.None = applyNone;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Functor_1 = __webpack_require__(4);
var Applicative = (function (_super) {
    __extends(Applicative, _super);
    function Applicative() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Applicative.prototype.map = function (f) {
        return this.map2(this.unit(function () { return undefined; }))(function (a, _) { return f(a); });
    };
    return Applicative;
}(Functor_1.default));
exports.default = Applicative;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Functor = (function () {
    function Functor() {
    }
    return Functor;
}());
exports.default = Functor;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Monad_1 = __webpack_require__(0);
var Cons = (function (_super) {
    __extends(Cons, _super);
    function Cons(h, t) {
        var _this = _super.call(this) || this;
        _this.h = h;
        _this.t = t;
        return _this;
    }
    Cons.prototype.unit = function (a) {
        return cons(a, function () { return null; });
    };
    Cons.prototype.flatMap = function (f) {
        return this.foldRight(null)(function (h, t) {
            return cons(function () { return f(h).h(); }, t);
        });
    };
    Cons.prototype.toArray = function () {
        var self = this;
        function go(l, acc) {
            if (l == null)
                return acc;
            return go(l.t(), acc.concat(l.h()));
        }
        return go(this, []);
    };
    Cons.prototype.foldRight = function (z) {
        var _this = this;
        return function (f) {
            return f(_this.h(), function () {
                var t = _this.t();
                return t == null ? null : t.foldRight(z)(f);
            });
        };
    };
    Cons.prototype.map = function (f) {
        return _super.prototype.map.call(this, f);
    };
    Cons.prototype.unshift = function (a) {
        var _this = this;
        return cons(function () { return a; }, function () { return _this; });
    };
    return Cons;
}(Monad_1.default));
function cons(hd, tl) {
    return new Cons(function () { return hd(); }, function () { return tl(); });
}
function apply() {
    var as = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        as[_i] = arguments[_i];
    }
    if (as.length == 0)
        return null;
    return cons(function () { return as[0]; }, function () { return apply.apply(void 0, as.slice(1)); });
}
exports.List = apply;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Operators = (function () {
    function Operators() {
    }
    Operators.prototype.map = function (f) {
        return function (fa) { return fa.map(f); };
    };
    Operators.prototype.map2 = function (f) {
        return function (fa) { return function (fb) {
            return fa.map2(fb)(f);
        }; };
    };
    return Operators;
}());
exports.Operators = Operators;
exports.ops = new Operators();


/***/ })
/******/ ])));