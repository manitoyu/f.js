const path = require('path')

module.exports = {
	entry: {
		index: './src/index.ts'
	},
	output: {
		filename: 'f.js',
		path: path.join(__dirname, 'dist'),
		publicPath: '/',
		// libraryTarget: 'commonjs'
	},
	devServer: {
		host: 'f-js.manitoyu.com',
		port: 4000,
		contentBase: path.join(__dirname, 'examples')
	},
	resolve: {
		extensions: [".js", ".ts"]
	},
	module: {
		rules: [
			{ test: /\.ts/, use: 'ts-loader' }
		]
	}
}